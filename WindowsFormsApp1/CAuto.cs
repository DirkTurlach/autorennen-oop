﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class CAuto
    {
        // Felder - Eigenschaften der Fahrzeuge
        public int x, y; // x- und y-Position
        public float va; // aktuelle Geschwindigkeit
        public Color farbe; // Lackierung

        // Methoden - Fähigkeiten
        public void Schneller(float dv)
        {
            va += dv;
            if (va > 100) va = 100;
        }

        public void Bremsen(float dv)
        {
            va -= dv;
            if (va <= 0) va = 0;
        }

        public void Zeichnen(Graphics g, Color colr)
        {
            Brush b = new SolidBrush(colr);
            Pen p = new Pen(colr);
            g.FillRectangle(b, x + 10, y + 5, 30, 20); // linkes Hinterrad
            g.FillRectangle(b, x + 10, y + 66, 30, 20); // rechtes Hinterrad
            g.FillRectangle(b, x + 80, y + 10, 30, 15); // linkes Vorderrad
            g.FillRectangle(b, x + 80, y + 66, 30, 15); // rechtes Vorderrad
            g.DrawLine(p, x + 25, y + 25, x + 25, y + 30); // linke Hinterachse
            g.DrawLine(p, x + 25, y + 60, x + 25, y + 65); // rechte Hinterachse
            g.DrawLine(p, x + 95, y + 20, x + 95, y + 35); // linke Vorderachse
            g.DrawLine(p, x + 95, y + 55, x + 95, y + 65); // rechte Vorderachse
            g.DrawLine(p, x, y + 30, x + 70, y + 30); // linke Karosserieseite
            g.DrawLine(p, x + 70, y + 30, x + 110, y + 40);
            g.DrawLine(p, x + 110, y + 40, x + 110, y + 50); // vordere Stoßstange
            g.DrawLine(p, x + 70, y + 60, x, y + 60); // rechte Karosserieseite
            g.DrawLine(p, x + 70, y + 60, x + 110, y + 50);
            g.DrawLine(p, x, y + 60, x, y + 30); // hintere Stoßstange
            g.FillEllipse(b, x + 45, y + 35, 20, 20); // Cockpit
        }

        #region Konstruktor
        //public CAuto();

        public CAuto(int xpos, int ypos, Color colr)
        {
            x = xpos; y = ypos; // Objekt initialisieren
            farbe = colr; // ...
        }
        #region
    }
}
