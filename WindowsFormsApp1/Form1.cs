﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        // Opjektvariablen erzeugen
        //private CAuto Auto1 = new CAuto(); // Mit leerem Konstruktor
        //private CAuto Auto2 = new CAuto();

        //Deklaration für Konstruktor mit Parametern
        private CAuto Auto1 = null;
        private CAuto Auto2 = null;

        // Grafikopjekte und Parameter der Rennbahn
        private Graphics g; // Grafikobjekt, auf dem gezeichnet wird
        private int xb = 1000; // außerhalb des Sichtbereichs liegender Streckenabschnitt
        private int xm; // Lage der Startlinie

        public Form1()
        {
            InitializeComponent();

            //Grafikobjekt für das Formular erzeugen und die Formularmitte feststellen:
            g = this.CreateGraphics();
            xm = this.Size.Width / 2;

            Auto1 = new CAuto(xm - 110, 10, Color.Red);
            Auto2 = new CAuto(xm - 110, 10, Color.Blue);

            //Die Eigenschaften jedes einzelnen Auto - Objekts werden im Folgenden auf ihre Anfangswerte eingestellt.
            ////Der rote Ferrari:
            //Auto1.x = xm - 110; // an Startlinie ausrichten
            //Auto1.y = 10; // obere Fahrbahn
            //Auto1.farbe = Color.Red;

            ////Der blaue BMW:
            //Auto2.x = xm - 110; // an Startlinie ausrichten
            //Auto2.y = 110; // untere Fahrbahn
            //Auto2.farbe = Color.Blue;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Auto1.Bremsen(1F);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Auto1.Schneller(0.5F);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Auto2.Bremsen(1F);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            Auto2.Schneller(0.5F);
        }

        //Optische Anzeige der Autos (Übergabeparameter für die folgende Methode ist ein beliebiges Objekt vom Typ CAuto) :
        private void anzeigen(CAuto auto) // Anzeigeroutine für ein Auto-Objekt
        {
            auto.Zeichnen(g, this.BackColor); // alte Position übermalen
            auto.x += Convert.ToInt32(auto.va); // Verschieben der x-Position
            if (auto.x >= this.Width) auto.x = -xb; // Rand erreicht
            auto.Zeichnen(g, auto.farbe); // an neuer Position zeichnen
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            anzeigen(Auto1);
            anzeigen(Auto2);
        }

        //Für den periodischen Aufruf nach dem Prinzip "Flimmerkiste" ist der Timer zuständig:

    }
}
